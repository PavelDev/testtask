package com.example.pavel.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.example.pavel.BuildConfig;
import com.example.pavel.service.response.callback.OnRequestFailure;
import com.example.pavel.utils.PrefUtil;
import com.squareup.picasso.Picasso;

/**
 * Created on 11.01.2018.
 */

abstract class BaseAPIActivity extends AppCompatActivity implements OnRequestFailure {

    View mProgressView;
    View mContentView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.DEBUG) {
            Picasso.with(this).setIndicatorsEnabled(true);
            Picasso.with(this).setLoggingEnabled(true);
        }
    }

    @Override
    public void onRequestFailure(Throwable t) {
        showProgress(false);
        Toast.makeText(getBaseContext(), "error: " + t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void unauthorized(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
        getPrefs().clear();
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    @NonNull
    PrefUtil getPrefs() {
        return new PrefUtil(getApplicationContext());
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
        mContentView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mContentView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
