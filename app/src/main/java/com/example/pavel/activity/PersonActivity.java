package com.example.pavel.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.pavel.R;
import com.example.pavel.service.response.model.PersonData;
import com.example.pavel.widget.MyImageView;
import com.squareup.picasso.Picasso;

public class PersonActivity extends AppCompatActivity {

    MyImageView personImage;
    private TextView personNumbers;
    private TextView personEmails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PersonData person = getIntent().getParcelableExtra("person");


        setContentView(R.layout.activity_person);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        personImage = findViewById(R.id.personImage);
        personNumbers = findViewById(R.id.personNumbers);
        personEmails = findViewById(R.id.personEmails);

        if (person != null) {
            if (person.pictureId != null) {
                Picasso.with(getApplicationContext()).load(person.pictureId.pictures._512).into(personImage);
            }
            personNumbers.setText(getString(R.string.person_phone_numbers) + (person.phone));
            personEmails.setText(getString(R.string.person_emails) + (person.email));
        }


    }

}
