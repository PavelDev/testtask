package com.example.pavel.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.pavel.R;
import com.example.pavel.adapters.PersonAdapter;
import com.example.pavel.service.APIService;
import com.example.pavel.service.response.PersonsResponse;
import com.example.pavel.service.response.callback.PersonsCallback;
import com.example.pavel.service.response.model.PersonData;

public class PersonListActivity extends BaseAPIActivity implements AdapterView.OnItemClickListener {

    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mListView = findViewById(R.id.personList);
        mListView.setOnItemClickListener(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "logged out", Toast.LENGTH_SHORT).show();
                getPrefs().clear();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
        });

        mContentView = findViewById(R.id.content);
        mProgressView = findViewById(R.id.progress);

        APIService.getInstance().getPersonList(getApplicationContext(), new PersonsCallback(this));


    }

    public void onPersonsLoaded(PersonsResponse body) {
        mListView.setAdapter(new PersonAdapter(getApplicationContext(), R.layout.item_person, body.getData()));
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        PersonData person = ((PersonAdapter) adapterView.getAdapter()).getItem(i);
        if (person == null) {
            return;
        }
        Toast.makeText(getBaseContext(), "name: " + person.name, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), PersonActivity.class);
        intent.putExtra("person", person);
        startActivity(intent);
    }
}
