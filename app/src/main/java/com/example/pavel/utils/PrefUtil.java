package com.example.pavel.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


// this class is a shortened tinyDB -> https://github.com/kcochibili/TinyDB--Android-Shared-Preferences-Turbo/blob/master/TinyDB.java

public class PrefUtil {

    private SharedPreferences preferences;
    private String PREF_API_TOKEN = "_api_token";

    public PrefUtil(Context appContext) {
        preferences = PreferenceManager.getDefaultSharedPreferences(appContext);
    }


    public String getString(String key) {
        return preferences.getString(key, null);
    }

    public void putString(String key, String value) {
        checkForNullKey(key);
        checkForNullValue(value);
        preferences.edit().putString(key, value).apply();
    }

    /**
     * Clear SharedPreferences (remove everything)
     */
    public void clear() {
        preferences.edit().clear().apply();
    }


    /**
     * null keys would corrupt the shared pref file and make them unreadable this is a preventive measure
     */
    public void checkForNullKey(String key) {
        if (key == null) {
            throw new NullPointerException();
        }
    }

    /**
     * null keys would corrupt the shared pref file and make them unreadable this is a preventive measure
     */
    public void checkForNullValue(String value) {
        if (value == null) {
            throw new NullPointerException();
        }
    }

    public String getApiToken() {
        return getString(PREF_API_TOKEN);
    }

    public void setApiToken(String apiToken) {
        putString(PREF_API_TOKEN, apiToken);
    }
}
