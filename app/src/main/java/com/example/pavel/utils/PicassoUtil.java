package com.example.pavel.utils;

import android.content.Context;

import com.squareup.picasso.Downloader;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

/**
 * Created on 11.01.2018.
 */

public class PicassoUtil {
    private static Picasso singleton;
    private static String CACHE_NAME = "_cache";

    public static Picasso getInstance(Context context) {
        if (singleton == null) {
            synchronized (PicassoUtil.class) {
                if (singleton == null) {
                    Context appContext = context.getApplicationContext();

                    File cacheDirectory = new File(context.getCacheDir(), CACHE_NAME);
                    Cache sdkApiCache = new Cache(cacheDirectory, 5 * 1024 * 1024);

                    OkHttpClient okHttpClient = new OkHttpClient.Builder()
                            .cache(sdkApiCache)
                            .addNetworkInterceptor(new Interceptor() {
                                @Override
                                public okhttp3.Response intercept(Chain chain) throws IOException {
                                    okhttp3.Response originalResponse = chain.proceed(chain.request());

                                    return originalResponse
                                            .newBuilder()
                                            .removeHeader("pragma")
                                            .header("Cache-Control", "max-age=" + (60 * 60 * 24 * 30))
                                            .build();
                                }
                            })
                            .retryOnConnectionFailure(false)
                            .build();

//                    Downloader downloader = new OkHttpDownloader(okHttpClient);

//                    singleton = new Picasso.Builder(appContext)
//                            .downloader(downloader)
//                            .build();
                }
            }
        }
        return singleton;
    }
}
