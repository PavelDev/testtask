package com.example.pavel.widget;


import android.content.Context;
import android.util.AttributeSet;

/**
 * Created on 11.01.2018.
 */

public class MyImageView extends android.support.v7.widget.AppCompatImageView {
    public MyImageView(Context context) {
        super(context);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setImageUrl(String url) {
        // this class is to handle different image sizes depending on actual view size
    }
}
