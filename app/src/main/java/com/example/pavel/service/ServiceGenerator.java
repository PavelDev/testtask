package com.example.pavel.service;

import android.content.Context;
import android.util.Log;

import com.example.pavel.BuildConfig;
import com.example.pavel.R;
import com.example.pavel.utils.PrefUtil;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created on 11.01.2018.
 */

public class ServiceGenerator {
    public static <S, I> S createService(Context context, Class<S> serviceClass) {
        String apiUrl = context.getString(R.string.api_url);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new ApiKeyInterceptor(context));

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor(new HttpLogger());
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(logInterceptor);
        }

        OkHttpClient client = httpClient.build();
        GsonConverterFactory factory = GsonConverterFactory.create();
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(apiUrl)
                .addConverterFactory(factory);
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }


    private static class ApiKeyInterceptor implements Interceptor {

        private final Context context;

        public ApiKeyInterceptor(Context context) {
            this.context = context;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request;

            HttpUrl url = original.url().newBuilder()
                    .setQueryParameter("api_token", context == null ? null : new PrefUtil(context).getApiToken())
                    .build();
            request = original.newBuilder().url(url).build();

            return chain.proceed(request);
        }
    }


    private static class HttpLogger implements HttpLoggingInterceptor.Logger {

        @Override
        public void log(String message) {
            Log.d("SERVICE", message);
        }

    }
}
