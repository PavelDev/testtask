package com.example.pavel.service.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 11.01.2018.
 */

public class UserData {
    @SerializedName("user_id")
    public Integer userId;
    @SerializedName("api_token")
    public String apiToken;

    // unneeded fields are ignored and not present
}
