package com.example.pavel.service.response.callback;

import android.support.annotation.NonNull;

import com.example.pavel.activity.LoginActivity;
import com.example.pavel.service.response.AuthResponse;
import com.example.pavel.service.response.BaseResponse;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created on 11.01.2018.
 */

public class AuthCallback extends BaseCallback<LoginActivity, AuthResponse> {
    public AuthCallback(LoginActivity reference) {
        super(reference);
    }

    @Override
    public void onResponse(@NonNull Call<AuthResponse> call, @NonNull Response<AuthResponse> response) {
        if (!isSuccess(call, response)) {
            return;
        }

        if (weakReference.get() != null) {
            weakReference.get().onUserAuthenticated(response.body());
        }
    }

}
