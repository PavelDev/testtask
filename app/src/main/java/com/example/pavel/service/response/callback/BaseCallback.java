package com.example.pavel.service.response.callback;

import android.support.annotation.NonNull;

import com.example.pavel.service.response.BaseResponse;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created on 11.01.2018.
 */

abstract class BaseCallback<T extends OnRequestFailure, R extends BaseResponse> implements Callback<R> {
    protected final WeakReference<T> weakReference;

    BaseCallback(T reference) {
        weakReference = new WeakReference<>(reference);
    }

    @Override
    public void onFailure(@NonNull Call<R> call, @NonNull Throwable t) {
        t.printStackTrace();
        if (weakReference.get() != null) {
            weakReference.get().onRequestFailure(t);
        }
    }


    boolean isSuccess(Call<R> call, Response<R> response) {
        R body = response.body();
        if (response.code() == 401) {
            if (weakReference.get() != null) {
                weakReference.get().unauthorized(response.message());
            }
            return false;
        } else {
            if (body == null) {
                onFailure(call, new Throwable("body == null"));
                return false;
            } else if (!body.isSuccess()) {
                onFailure(call, new Throwable(body.getError()));
                return false;
            }
        }
        return true;
    }
}
