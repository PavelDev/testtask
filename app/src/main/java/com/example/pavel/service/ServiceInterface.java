package com.example.pavel.service;

import com.example.pavel.service.response.AuthResponse;
import com.example.pavel.service.response.PersonsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created on 11.01.2018.
 */

public interface ServiceInterface {
    @POST("v1/authorizations")
    Call<AuthResponse> postAuthentication(@Body AuthBody authBody);

    @GET("v1/persons")
    Call<PersonsResponse> getPersonList(@Query("start") Integer start);
}
