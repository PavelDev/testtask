package com.example.pavel.service.response.callback;

/**
 * Created on 11.01.2018.
 */

public interface OnRequestFailure {
    void onRequestFailure(Throwable t);
    void unauthorized(String msg);
}
