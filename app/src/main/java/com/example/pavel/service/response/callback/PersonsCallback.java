package com.example.pavel.service.response.callback;

import android.support.annotation.NonNull;

import com.example.pavel.activity.PersonListActivity;
import com.example.pavel.service.response.AuthResponse;
import com.example.pavel.service.response.PersonsResponse;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created on 11.01.2018.
 */

public class PersonsCallback extends BaseCallback<PersonListActivity, PersonsResponse> {
    public PersonsCallback(PersonListActivity reference) {
        super(reference);
    }

    @Override
    public void onResponse(@NonNull Call<PersonsResponse> call, @NonNull Response<PersonsResponse> response) {
        if (!isSuccess(call, response)) {
            return;
        }

        if (weakReference.get() != null) {
            weakReference.get().onPersonsLoaded(response.body());
        }
    }

}
