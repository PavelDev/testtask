package com.example.pavel.service.response.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created on 11.01.2018.
 */

public class PictureId implements Parcelable {
    public Pictures pictures;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.pictures, flags);
    }

    public PictureId() {
    }

    protected PictureId(Parcel in) {
        this.pictures = in.readParcelable(Pictures.class.getClassLoader());
    }

    public static final Creator<PictureId> CREATOR = new Creator<PictureId>() {
        @Override
        public PictureId createFromParcel(Parcel source) {
            return new PictureId(source);
        }

        @Override
        public PictureId[] newArray(int size) {
            return new PictureId[size];
        }
    };
}
