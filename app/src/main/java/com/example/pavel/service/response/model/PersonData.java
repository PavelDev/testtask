package com.example.pavel.service.response.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 11.01.2018.
 */

public class PersonData implements Parcelable {

    public String name = "testname";

    public List<ContactData> phone;
    public List<ContactData> email;

    @SerializedName("picture_id")
    public PictureId pictureId;

    @Override
    public String toString() {
        return name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeTypedList(this.phone);
        dest.writeTypedList(this.email);
        dest.writeParcelable(this.pictureId, flags);
    }

    public PersonData() {
    }

    protected PersonData(Parcel in) {
        this.name = in.readString();
        this.phone = in.createTypedArrayList(ContactData.CREATOR);
        this.email = in.createTypedArrayList(ContactData.CREATOR);
        this.pictureId = in.readParcelable(PictureId.class.getClassLoader());
    }

    public static final Creator<PersonData> CREATOR = new Creator<PersonData>() {
        @Override
        public PersonData createFromParcel(Parcel source) {
            return new PersonData(source);
        }

        @Override
        public PersonData[] newArray(int size) {
            return new PersonData[size];
        }
    };
}
