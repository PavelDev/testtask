package com.example.pavel.service.response.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created on 11.01.2018.
 */

public class ContactData implements Parcelable {

    public String label;
    public String value;
    public boolean primary;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.label);
        dest.writeString(this.value);
        dest.writeByte(this.primary ? (byte) 1 : (byte) 0);
    }

    public ContactData() {
    }

    protected ContactData(Parcel in) {
        this.label = in.readString();
        this.value = in.readString();
        this.primary = in.readByte() != 0;
    }

    public static final Creator<ContactData> CREATOR = new Creator<ContactData>() {
        @Override
        public ContactData createFromParcel(Parcel source) {
            return new ContactData(source);
        }

        @Override
        public ContactData[] newArray(int size) {
            return new ContactData[size];
        }
    };

    @Override
    public String toString() {
        return value;
    }
}
