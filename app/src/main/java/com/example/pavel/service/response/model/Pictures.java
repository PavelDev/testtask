package com.example.pavel.service.response.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 11.01.2018.
 */

public class Pictures implements Parcelable {
    @SerializedName("128")
    public String _128;
    @SerializedName("512")
    public String _512;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this._128);
        dest.writeString(this._512);
    }

    public Pictures() {
    }

    protected Pictures(Parcel in) {
        this._128 = in.readString();
        this._512 = in.readString();
    }

    public static final Creator<Pictures> CREATOR = new Creator<Pictures>() {
        @Override
        public Pictures createFromParcel(Parcel source) {
            return new Pictures(source);
        }

        @Override
        public Pictures[] newArray(int size) {
            return new Pictures[size];
        }
    };
}
