package com.example.pavel.service.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created on 11.01.2018.
 */

public class AdditionalData {
    public Pagination pagination;

    public class Pagination {

        @SerializedName("start")
        public Integer start;
        @SerializedName("limit")
        public Integer limit;
        @SerializedName("more_items_in_collection")
        public boolean moreItemsInCollection;

    }

}
