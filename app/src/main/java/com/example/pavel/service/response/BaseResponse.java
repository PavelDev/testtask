package com.example.pavel.service.response;

import com.example.pavel.service.response.model.AdditionalData;

import java.util.List;

/**
 * Created on 11.01.2018.
 */

public abstract class BaseResponse<TYPE> {
    private boolean success;
    private List<TYPE> data;
    private AdditionalData additionalData;

    public String getError() {
        return error;
    }

    private String error;

    public boolean isSuccess() {
        return success;
    }

    public List<TYPE> getData() {
        return data;
    }

    public AdditionalData getAdditionalData() {
        return additionalData;
    }
}
