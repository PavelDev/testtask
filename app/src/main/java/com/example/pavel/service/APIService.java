package com.example.pavel.service;

import android.content.Context;

import com.example.pavel.service.response.callback.AuthCallback;
import com.example.pavel.service.response.callback.PersonsCallback;

/**
 * Created on 11.01.2018.
 */

public class APIService {

    private static APIService instance;
    private ServiceInterface service;

    public static APIService getInstance() {
        if (instance == null) {
            instance = new APIService();
        }
        return instance;
    }

    private ServiceInterface getService(Context context) {
        if (service == null) {
            service = ServiceGenerator.createService(context,
                    ServiceInterface.class);
        }
        return service;
    }

    public void postAuthentication(Context context, String email, String password, AuthCallback callback) {
        getService(context).postAuthentication(new AuthBody(email, password)).enqueue(callback);
    }

    public void getPersonList(Context context, PersonsCallback callback) {
        getService(context).getPersonList(null).enqueue(callback);
    }

}
