package com.example.pavel.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ArrayAdapter;

import com.example.pavel.service.response.model.PersonData;

import java.util.List;

/**
 * Created on 11.01.2018.
 */

public class PersonAdapter extends ArrayAdapter<PersonData> {
    public PersonAdapter(@NonNull Context context, int resource, @NonNull List<PersonData> objects) {
        super(context, resource, objects);
    }
}
